import math
def compute_area_square(area):
    return area*area
def compute_area_rectangle(base, height):
    return base*height
def compute_area_circle(radious):
    return math.pi*radious

print(compute_area_square(2))
print(compute_area_rectangle(2,6))
print(compute_area_circle(2))

def menu():
    print("Welcome to the Area Calculator")
    print("1. Calculate square area")
    print("2. Calculate rectangle area")
    print("3. Calculate circle area")
    print("quit To Exit")
    option = input("Write the number of the option you want (Write \"quit\" to exit")
    if option == "1":
        #compute_area_square(area)
